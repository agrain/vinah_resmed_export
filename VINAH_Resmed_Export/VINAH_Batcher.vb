﻿Public Class VINAH_Batcher
    Dim EM As ResMedEntities
    Dim FileStartTime As DateTime
    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        '   Trigger batch creation
        FileStartTime = Now
        EM = New ResMedEntities
        Dim VINAHFile As New HL7_File()
        '	gather event messages by time period
        Dim Thenn As DateTime = Now.AddDays(-77)
        Dim EventsByDate = (From events In EM.View_Vin_Events_ByTime Where events.Timestamp > Thenn And events.Timestamp < Now Select events).ToList

        '	group messages for each patient
        Dim EventsByPatient As List(Of Decimal) = (From ebd In EventsByDate Select ebd.RClientID Distinct).ToList
        For Each pat In EventsByPatient
            Dim PBatch As New HL7_Batch(pat)
            Dim PatientEvents = (From ev In EventsByDate Where ev.RClientID = pat Select ev Order By ev.Timestamp).ToList
            Dim PatientSentEvents = (From sent In EM.View_Vin_Messages_Acked Where sent.RClientID = pat Select sent).ToList
            For Each patevn In PatientEvents
                '	filter out messages that have been parsed already
                Dim occurred = (From pse In PatientSentEvents Where pse.REntityID = patevn.EventID And pse.REntityType = patevn.EventType Select pse).Count
                Dim msg As HL7_Message = Nothing
                If occurred = 0 Then
                    BuildEventMsg(patevn, PBatch, patevn.EventType)
                End If
            Next
            If PBatch.Valid Then
                VINAHFile.Batches.Add(PBatch)
            End If
        Next
        EM = Nothing
    End Sub

    Private Function BuildEventMsg(ByVal evnt As View_Vin_Events_ByTime, ByVal Batch As HL7_Batch, EventType As Char, Optional ByVal client As Vin_H_Client = Nothing, Optional ByVal episode As Vin_H_Episode = Nothing) As Boolean
        Dim success As Boolean = True
        Dim o As Object = PopulateEvent(evnt, EventType, client, episode)
        Dim ValidEntity As Boolean = ValidateEntity(o) ' True        'WHAT MAKES A REFERRAL VALID?         'check referraltype is either "I" or "O"

        Dim ent = EntityCreate(evnt, EventType, o, client, episode)
        Dim msg As HL7_Message = Nothing

        'If the entity has been changed since last ack - reset it to previous state
        If ent.LastUpdateDate < FileStartTime Then
            If ent.AckStatus <> ent.PendingStatus Then
                If ent.LastAckDate IsNot Nothing Then ent.LastUpdateDate = ent.LastAckDate
                If ent.AckStatus = "N" Then
                    ent.PendingStatus = "B"
                Else
                    ent.PendingStatus = ent.AckStatus
                End If
            End If
        End If

        If ValidEntity Then
            'Insert OR Update Entity
            If ent.PendingStatus = "D" Or ent.PendingStatus = "B" Then
                'INSERT
                success = MSGInsertEntity(msg, EventType, evnt, Batch, client, episode)
                ent.PendingStatus = "A"
            Else
                'UPDATE
                success = MSGUpdateEntity(msg, EventType, evnt, Batch, client, episode)
                ent.LastUpdateDate = Now
                ent.PendingStatus = "U"
            End If
        Else
            'DELETE OR Omitt message
            If ent.PendingStatus = "D" Or ent.PendingStatus = "B" Then
                'IGNORE
            Else
                'DELETE
                success = MSGDeleteEntity(msg, EventType, evnt, Batch, client, episode)
                ent.LastUpdateDate = Now
                ent.PendingStatus = "D"
            End If
        End If

        EM.SaveChanges()
        If msg IsNot Nothing Then
            MSGComplete(msg, EventType, o, client)
        End If
        Batch.Messages.Add(msg)
        Return success
    End Function

    ''' <summary>
    ''' Search for the Event for this EventByTime
    ''' </summary>
    ''' <param name="evnt">EventByTime</param>
    ''' <param name="EventType"></param>
    ''' <param name="Client"></param>
    ''' <param name="Episode"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private Function PopulateEvent(ByVal evnt As View_Vin_Events_ByTime, EventType As Char, ByRef Client As Vin_H_Client, ByRef Episode As Vin_H_Episode) As Object
        Dim o As Object = Nothing
        Select Case EventType
            Case "C"
                o = (From c In EM.Vin_H_Contact Where c.RContactID = evnt.EventID Select c).FirstOrDefault
                Client = LookupClient(o.rclientid)
                Episode = LookupEpisode(o.repisodeid)
            Case "E"
                If Episode Is Nothing Then
                    o = (From e In EM.Vin_H_Episode Where e.REpisodeID = evnt.EventID Select e).FirstOrDefault
                Else
                    o = Episode
                End If
                Client = LookupClient(o.rclientid)
            Case "I"
                o = (From i In EM.Vin_H_Referral_In Where i.RReferralInID = evnt.EventID Select i).FirstOrDefault
                Client = LookupClient(o.rclientid)
            Case "O"
            Case "R"
                o = (From r In EM.Vin_H_Referral_Out Where r.RReferralOutID = evnt.EventID Select r).FirstOrDefault
                Client = LookupClient(o.rclientid)
            Case "P"
                o = Client
        End Select
        Return o
    End Function
    Private Function ValidateEntity(ByRef o As Object)
        Return True
    End Function
#Region "INSERT"
    Private Function MSGInsertEntity(ByRef msg As HL7_Message, ByVal Etype As Char, ByVal evnt As View_Vin_Events_ByTime, ByVal Batch As HL7_Batch, ByVal Client As Vin_H_Client, ByVal Episode As Vin_H_Episode) As Boolean
        Dim success As Boolean = True
        Select Case Etype
            Case "C"
                success = BuildEventMsg(evnt, Batch, "E", Client, Episode)
                msg = New HL7_Message(HL7_Message.HL7_Types.ADT_A03_ContactInsert, evnt)
            Case "E"
                success = BuildEventMsg(evnt, Batch, "P", Client, Episode)
                msg = New HL7_Message(HL7_Message.HL7_Types.PPP_PCB_EpisodeInsert, evnt)
            Case "I"
                success = BuildEventMsg(evnt, Batch, "P", Client)
                msg = New HL7_Message(HL7_Message.HL7_Types.RRI_I12_ReferralInInsert, evnt)
            Case "O"
            Case "R"
                success = BuildEventMsg(evnt, Batch, "P", Client)
                msg = New HL7_Message(HL7_Message.HL7_Types.REF_I12_ReferralOutInsert, evnt)
            Case "P"
                msg = New HL7_Message(HL7_Message.HL7_Types.ADT_A04_ClientInsert, evnt)
        End Select
        Return success
    End Function
#End Region
#Region "UPDATE"
    Private Function MSGUpdateEntity(ByRef msg As HL7_Message, ByVal Etype As Char, ByVal evnt As View_Vin_Events_ByTime, ByVal Batch As HL7_Batch, ByVal Client As Vin_H_Client, ByVal Episode As Vin_H_Episode) As Boolean
        Dim success As Boolean = True
        Select Case Etype
            Case "C"
                success = BuildEventMsg(evnt, Batch, "E", Client, Episode)
                msg = New HL7_Message(HL7_Message.HL7_Types.ADT_A08_ContactUpdate, evnt)
            Case "E"
                msg = New HL7_Message(HL7_Message.HL7_Types.PPP_PCC_EpisodeUpdate, evnt)
            Case "I"
                success = BuildEventMsg(evnt, Batch, "P", Client)
                msg = New HL7_Message(HL7_Message.HL7_Types.RRI_I13_ReferralInUpdate, evnt)
            Case "O"
            Case "R"
                success = BuildEventMsg(evnt, Batch, "P", Client)
                msg = New HL7_Message(HL7_Message.HL7_Types.REF_I13_ReferralOutUpdate, evnt)
            Case "P"
                msg = New HL7_Message(HL7_Message.HL7_Types.ADT_A08_ClientUpdate, evnt)
        End Select
        Return success
    End Function
#End Region
#Region "DELETE"
    Private Function MSGDeleteEntity(ByRef msg As HL7_Message, ByVal Etype As Char, ByVal evnt As View_Vin_Events_ByTime, ByVal Batch As HL7_Batch, ByVal Client As Vin_H_Client, ByVal Episode As Vin_H_Episode) As Boolean
        Dim success As Boolean = True
        Select Case Etype
            Case "C"
                msg = New HL7_Message(HL7_Message.HL7_Types.ADT_A13_ContactDelete, evnt)
            Case "E"
                'Find Child Entities
                'GATHER CHILD CONTACTS
                'DELETE CHILD CONTACTS

                'A13
                'GATHER CHILD REFERRALS
                'I14
                'DELETE CHILD REFERRALS
                'DELETE EPISODE
                'PCD 
                msg = New HL7_Message(HL7_Message.HL7_Types.PPP_PCD_EpisodeDelete, evnt)
            Case "I"
                msg = New HL7_Message(HL7_Message.HL7_Types.RRI_I14_ReferralInDelete, evnt)
            Case "O"
            Case "R"
                msg = New HL7_Message(HL7_Message.HL7_Types.REF_I14_ReferralOutDelete, evnt)
            Case "P"
                'IGNORE
        End Select
        Return success
    End Function
#End Region

#Region "COMPLETE MSG"
    Private Function MSGComplete(ByRef msg As HL7_Message, ByVal Etype As Char, ByVal o As Object, ByVal Client As Vin_H_Client) As Boolean
        Dim success As Boolean = True
        Select Case Etype
            Case "C"
                msg.ImportSegment(Client, "PID")
                msg.ImportSegment(o, "PV1")
                msg.ImportSegment(o, "PV2")
                msg.ImportSegment(o, "PR1")
                msg.ImportSegment(o, "ROL")
            Case "E"
                msg.ImportSegment(Client, "PID")
                msg.ImportSegment(o, "PV1")
                msg.ImportSegment(o, "PV2")
                msg.ImportSegment(o, "OBX")
                msg.ImportSegment(o, "PTH")
                msg.ImportSegment(o, "PRB")
            Case "O"
            Case "I", "R"
                msg.ImportSegment(Client, "PID")
                msg.ImportSegment(o, "PRD")
                msg.ImportSegment(o, "RF1")
            Case "P"
                msg.ImportSegment(o, "PID")
                msg.ImportSegment(o, "NK1")
                msg.ImportSegment(o, "PD1")
        End Select
        Return success
    End Function
#End Region

#Region "ENTITY MANAGMENT"
    Private Function EntityCreate(ByRef evnt As View_Vin_Events_ByTime, ByRef oType As Char, ByVal o As Object, ByVal client As Vin_H_Client, ByVal episode As Vin_H_Episode) As Vin_Trk_Entity
        Dim EntityBlueprint As Vin_Trk_Entity = Nothing
        If oType = "P" Then
            EntityBlueprint = EntityVINAHstatus(client)
        ElseIf oType = "E" And episode IsNot Nothing Then
            EntityBlueprint = EntityVINAHstatus(episode)
        Else
            EntityBlueprint = EntityVINAHstatus(o)
        End If

        If EntityBlueprint Is Nothing Then
            Select Case oType
                Case "C"
                    EntityBlueprint = New Vin_Trk_Entity With {.REntityID = evnt.EventID, .RClientID = client.RClientID, .LastUpdateDate = Now, .REntity_Type = oType}
                Case "E"
                    If episode Is Nothing Then
                        EntityBlueprint = New Vin_Trk_Entity With {.REntityID = evnt.EventID, .RClientID = client.RClientID, .LastUpdateDate = Now, .REntity_Type = oType}
                    Else
                        EntityBlueprint = New Vin_Trk_Entity With {.REntityID = episode.REpisodeID, .RClientID = client.RClientID, .LastUpdateDate = Now, .REntity_Type = oType}
                    End If
                Case "I"
                    EntityBlueprint = New Vin_Trk_Entity With {.REntityID = evnt.EventID, .RClientID = client.RClientID, .LastUpdateDate = Now, .REntity_Type = oType}
                Case "O"
                Case "R"
                    EntityBlueprint = New Vin_Trk_Entity With {.REntityID = evnt.EventID, .RClientID = client.RClientID, .LastUpdateDate = Now, .REntity_Type = oType}
                Case "P"
                    EntityBlueprint = New Vin_Trk_Entity With {.REntityID = client.RClientID, .RClientID = client.RClientID, .LastUpdateDate = Now, .REntity_Type = oType}
            End Select

            If EntityBlueprint IsNot Nothing Then
                EntityBlueprint.PendingStatus = "B"
                EntityBlueprint.AckStatus = "N"
                EM.Vin_Trk_Entity.Add(EntityBlueprint)
                EM.SaveChanges()
            End If
        End If
        Return EntityBlueprint
    End Function

#Region "ENTITY VINAH STATUS"
    Private Function EntityVINAHstatus(ByVal o As Object) As Vin_Trk_Entity
        If o.GetType.Name.Contains("Vin_H_Client") Then
            Dim client As Vin_H_Client = o
            Dim ent = (From t In EM.Vin_Trk_Entity Where t.RClientID = client.RClientID And t.REntityID = client.RClientID And t.REntity_Type = "P" Select t).FirstOrDefault
            Return ent
        ElseIf o.GetType.Name.Contains("Vin_H_Episode") Then
            Dim episode As Vin_H_Episode = o
            Dim ent = (From t In EM.Vin_Trk_Entity Where t.RClientID = episode.RClientID And t.REntityID = episode.REpisodeID And t.REntity_Type = "E" Select t).FirstOrDefault
            Return ent
        ElseIf o.GetType.Name.Contains("Vin_H_Contact") Then
            Dim contact As Vin_H_Contact = o
            Dim ent = (From t In EM.Vin_Trk_Entity Where t.RClientID = contact.RClientID And t.REntityID = contact.RContactID And t.REntity_Type = "C" Select t).FirstOrDefault
            Return ent
        ElseIf o.GetType.Name.Contains("Vin_H_Referral_In") Then
            Dim refIn As Vin_H_Referral_In = o
            Dim ent = (From t In EM.Vin_Trk_Entity Where t.RClientID = refIn.RClientID And t.REntityID = refIn.RReferralInID And t.REntity_Type = "I" Select t).FirstOrDefault
            Return ent
        ElseIf o.GetType.Name.Contains("Vin_H_Referral_Out") Then
            Dim refOut As Vin_H_Referral_Out = o
            Dim ent = (From t In EM.Vin_Trk_Entity Where t.RClientID = refOut.RClientID And t.REntityID = refOut.RReferralOutID And t.REntity_Type = "R" Select t).FirstOrDefault
            Return ent
        ElseIf o.GetType.Name.Contains("Vin_H_Merge") Then
            Dim merge As Vin_H_Merge = o
            Dim ent = (From m In EM.Vin_Trk_Entity Where m.RClientID = merge.RClientID And m.REntityID = merge.RMergeID And m.REntity_Type = "M" Select m).FirstOrDefault
            Return ent
        Else
            Return Nothing
        End If
    End Function
#End Region
#End Region

#Region "RELATED ENTITIES"
    Private Function LookupClient(ByVal RClientID As Decimal) As Vin_H_Client
        Dim c As Vin_H_Client = Nothing
        c = EM.Vin_H_Client.Where(Function(cl As Vin_H_Client) cl.RClientID = RClientID).FirstOrDefault
        Return c
    End Function
    Private Function LookupEpisode(ByVal REpisodeID As Decimal) As Vin_H_Episode
        Dim c As Vin_H_Episode = Nothing
        c = EM.Vin_H_Episode.Where(Function(cl As Vin_H_Episode) cl.REpisodeID = REpisodeID).FirstOrDefault
        Return c
    End Function
    Private Function LookupContact(ByVal REpisodeID As Decimal) As Vin_H_Episode
        Dim c As Vin_H_Episode = Nothing
        c = EM.Vin_H_Episode.Where(Function(cl As Vin_H_Episode) cl.REpisodeID = REpisodeID).FirstOrDefault
        Return c
    End Function
    Private Function LookupReferralIn(ByVal RReferralInID As Decimal) As Vin_H_Referral_In
        Dim c As Vin_H_Referral_In = Nothing
        c = EM.Vin_H_Referral_In.Where(Function(cl As Vin_H_Referral_In) cl.RReferralInID = RReferralInID).FirstOrDefault
        Return c
    End Function
    Private Function LookupReferralOut(ByVal RReferralOutID As Decimal) As Vin_H_Referral_Out
        Dim c As Vin_H_Referral_Out = Nothing
        c = EM.Vin_H_Referral_Out.Where(Function(cl As Vin_H_Referral_Out) cl.RReferralOutID = RReferralOutID).FirstOrDefault
        Return c
    End Function
    Private Function LookupMerge(ByVal RMergeID As Decimal) As Vin_H_Merge
        Dim c As Vin_H_Merge = Nothing
        c = EM.Vin_H_Merge.Where(Function(cl As Vin_H_Merge) cl.RMergeID = RMergeID).FirstOrDefault
        Return c
    End Function
#End Region
End Class
