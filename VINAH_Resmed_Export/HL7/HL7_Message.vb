﻿Imports System.Text
Imports System.Reflection
Public Class HL7_Message
    Public Enum HL7_Types
        ADT_A04_ClientInsert
        ADT_A08_ClientUpdate
        ADT_A40_ClientMerge
        RRI_I12_ReferralInInsert
        RRI_I13_ReferralInUpdate
        RRI_I14_ReferralInDelete
        PPP_PCB_EpisodeInsert
        PPP_PCC_EpisodeUpdate
        PPP_PCD_EpisodeDelete
        ADT_A03_ContactInsert
        ADT_A08_ContactUpdate
        ADT_A13_ContactDelete
        REF_I12_ReferralOutInsert
        REF_I13_ReferralOutUpdate
        REF_I14_ReferralOutDelete
    End Enum
    Public Sub New(ByVal msgType As HL7_Types, ByVal evnt As View_Vin_Events_ByTime)
        z_msgTimestamp = Now
        z_TriggerEvent = evnt
        'BuildItem(New Coordinates("MSH_6_ReceivingFacility"), ReceivingFacility) STATIC
        BuildItem(New Coordinates("MSH_7_MessageTime"), msgTimestamp.ToString("yyyyMMddhhmmss"))
        BuildItem(New Coordinates("MSH_10_MessageControlID_mID"), "mcontrolid12345")

        Select Case msgType
            Case HL7_Types.ADT_A04_ClientInsert
                BuildItem(New Coordinates("MSH_9_1_MessageType_MessageType_MessageType"), "ADT")    'KEY
                BuildItem(New Coordinates("MSH_9_2_MessageType_EventType_MessageType"), "A01")
            Case HL7_Types.ADT_A08_ClientUpdate
                BuildItem(New Coordinates("MSH_9_1_MessageType_MessageType_MessageType"), "ADT")    'KEY
                BuildItem(New Coordinates("MSH_9_2_MessageType_EventType_MessageType"), "A08")
            Case HL7_Types.RRI_I12_ReferralInInsert
                BuildItem(New Coordinates("MSH_9_1_MessageType_MessageType_MessageType"), "RRI")    'KEY
                BuildItem(New Coordinates("MSH_9_2_MessageType_EventType_MessageType"), "I12")
            Case HL7_Types.RRI_I13_ReferralInUpdate
                BuildItem(New Coordinates("MSH_9_1_MessageType_MessageType_MessageType"), "RRI")    'KEY
                BuildItem(New Coordinates("MSH_9_2_MessageType_EventType_MessageType"), "I13")
            Case HL7_Types.RRI_I14_ReferralInDelete
                BuildItem(New Coordinates("MSH_9_1_MessageType_MessageType_MessageType"), "RRI")    'KEY
                BuildItem(New Coordinates("MSH_9_2_MessageType_EventType_MessageType"), "I14")
            Case HL7_Types.REF_I12_ReferralOutInsert
                BuildItem(New Coordinates("MSH_9_1_MessageType_MessageType_MessageType"), "RRI")    'KEY
                BuildItem(New Coordinates("MSH_9_2_MessageType_EventType_MessageType"), "I12")
            Case HL7_Types.REF_I13_ReferralOutUpdate
                BuildItem(New Coordinates("MSH_9_1_MessageType_MessageType_MessageType"), "RRI")    'KEY
                BuildItem(New Coordinates("MSH_9_2_MessageType_EventType_MessageType"), "I13")
            Case HL7_Types.REF_I14_ReferralOutDelete
                BuildItem(New Coordinates("MSH_9_1_MessageType_MessageType_MessageType"), "RRI")    'KEY
                BuildItem(New Coordinates("MSH_9_2_MessageType_EventType_MessageType"), "I14")
        End Select
    End Sub

    Private z_msgType As HL7_Types
    Public ReadOnly Property HL7MsgType As HL7_Types
        Get
            Return z_msgType
        End Get
    End Property
    Private z_msgTimestamp As DateTime
    Public ReadOnly Property msgTimestamp As DateTime
        Get
            Return z_msgTimestamp
        End Get
    End Property
    Private z_TriggerEvent As View_Vin_Events_ByTime
    Public ReadOnly Property TriggerEvent As View_Vin_Events_ByTime
        Get
            Return z_TriggerEvent
        End Get
    End Property

    Public Segments As New List(Of HL7_Segment)
    ''' <summary>
    ''' Adds or retrieves an existing Segment from the parent msg
    ''' </summary>
    ''' <param name="Seg"></param>
    ''' <param name="SegmentRepeats"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function AddSegment(ByVal Seg As HL7_Segment, Optional ByVal SegmentRepeats As Integer = -1) As HL7_Segment
        Dim segIndex As Integer = Segments.FindIndex(Function(s As HL7_Segment) s.segment_name = Seg.segment_name)
        If segIndex >= 0 Then
            Segments(segIndex) = Seg
        Else
            If SegmentRepeats = -1 Then
                'use the found segment
                Segments.Add(Seg)
            Else
                'find appropriate repetition of segment

            End If
        End If
        Return Seg
    End Function
    Public Function FindSegment(ByVal segName As String) As HL7_Segment
        Dim seg = Segments.Find(Function(s As HL7_Segment) s.segment_name = segName)
        Return seg
    End Function

    Public ReadOnly Property MessageContent As String
        Get
            Dim MessText As New StringBuilder
            For Each seg In Segments
                MessText.Append(seg.SegmentText)
                MessText.AppendLine()
            Next
            Return MessText.ToString
        End Get
    End Property

    ''' <summary>
    ''' Deconstructs the coordinates provided eg: "PID-3.1_PrimaryAustinUR" (seg)-(feild)-(carrot)_(description)
    ''' </summary>
    ''' <param name="Coordiantes">(seg)-(feild)-(carrot)_(description)</param>
    ''' <param name="Value"></param>
    ''' <remarks></remarks>
    Public Sub BuildItem(ByVal c As Coordinates, ByVal Value As String, Optional ByVal SegmentRepeats As Integer = -1, Optional ByVal FieldRepeats As String = Nothing)
        'Carrot
        Dim car As New HL7_Carrot(c.Carrot, c.CarrotName, Value, c)

        'Segment
        Dim seg As HL7_Segment = FindSegment(c.Segment)
        If seg IsNot Nothing Then
            'found the segment
            If SegmentRepeats Then
                'find the correct repetition
                Dim lkjlj = 435
            End If
        Else
            seg = New HL7_Segment(c.Segment)
            Me.AddSegment(seg, SegmentRepeats)
        End If

        'Field
        Dim f As HL7_Field = seg.AllFields.Find(Function(fi As HL7_Field) fi.Name = c.FieldName And fi.Coords.Description = c.Description)
        If f IsNot Nothing Then
            Dim carrotExists = f.Carrots.Find(Function(fcar As HL7_Carrot) fcar.carrot_name = c.CarrotName)
            If carrotExists IsNot Nothing Then
                'add and create the new Field
                f = New HL7_Field(c.Field, c.FieldName, c)
                seg.AddField(f)
            End If
        Else
            'add and create the new Field
            f = New HL7_Field(c.Field, c.FieldName, c)
            seg.AddField(f)
        End If
        f.AddCarrot(car)

        PopulateTieInsByFieldName(c)
    End Sub


    Public Sub PopulateTieInsByFieldName(ByVal c As Coordinates)
        Dim DB As New ResMedEntities
        Dim TieIns = (From t In DB.Vin_Ref_TieIn Where t.TieInKey = c.FullDescription Select t).ToList
        For Each t In TieIns
            Dim tiCoords = New Coordinates(t.TieInAddress)
            BuildItem(tiCoords, t.TieInValue)
        Next
    End Sub

    Public Sub ImportSegment(ByVal o As Object, ByVal SegmentName As String)
        Dim props As PropertyInfo() = o.GetType().GetProperties()

        For Each prop As PropertyInfo In props
            If prop.PropertyType().Name = "String" And prop.Name.StartsWith(SegmentName & "_") Then
                Dim coords As New Coordinates(prop.Name)
                If prop.GetValue(o) <> "" Then
                    BuildItem(coords, prop.GetValue(o))
                End If
            End If
        Next
    End Sub

    Public Sub LogMessage(ByVal BatchID As Decimal)
        Dim EM As New ResMedEntities
        Dim newLog As New Vin_Log_Messages With {.RClientID = TriggerEvent.RClientID, .CreateDate = msgTimestamp, .REntityID = TriggerEvent.EventID, .REntityType = TriggerEvent.EventType, .Message = Me.MessageContent, .BatchID = BatchID}
    End Sub
End Class