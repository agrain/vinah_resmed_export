﻿Imports System.Text
Public Class Coordinates
    Public Sub New(ByVal coordstring As String)
        Try
            coordstring = coordstring.Replace(".", "_").Replace("-", "_")
            Dim namesplit = coordstring.Split("_")
            z_segment = namesplit(0)
            z_Field = Integer.Parse(namesplit(1))
            If namesplit.Count = 3 Then
                z_FieldName = namesplit(2)
                z_Description = namesplit(2)
            ElseIf namesplit.Count = 6 Then
                z_Carrot = Integer.Parse(namesplit(2))
                z_FieldName = namesplit(3)
                z_CarrotName = namesplit(4)
                z_Description = namesplit(5)
            End If
            z_FullDescription = coordstring
        Catch ex As Exception
            z_valid = False
        End Try
    End Sub

    Private z_valid As Boolean = True
    Public ReadOnly Property Valid As Boolean
        Get
            Return z_valid
        End Get
    End Property

    Private z_segment As String
    Public ReadOnly Property Segment As String
        Get
            Return z_segment
        End Get
    End Property

    Private z_Field As Integer
    Public ReadOnly Property Field As Integer
        Get
            Return z_Field
        End Get
    End Property

    Private z_Carrot As Integer
    Public ReadOnly Property Carrot As Integer
        Get
            Return z_Carrot
        End Get
    End Property

    Private z_FieldName As String
    Public ReadOnly Property FieldName As String
        Get
            Return z_FieldName
        End Get
    End Property

    Private z_CarrotName As String
    Public ReadOnly Property CarrotName As String
        Get
            Return z_CarrotName
        End Get
    End Property

    Private z_Description As String
    Public ReadOnly Property Description As String
        Get
            Return z_Description
        End Get
    End Property

    Private z_FullDescription As String
    Public ReadOnly Property FullDescription As String
        Get
            Return z_FullDescription
        End Get
    End Property
End Class
