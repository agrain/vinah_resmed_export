﻿Imports System.Text
Public Class HL7_Field
    Public Sub New(ByVal FieldIndex As Integer, ByVal FieldName As String, ByVal coords As Coordinates)
        z_index = FieldIndex
        z_Name = FieldName
        z_Coords = coords
    End Sub
    Private z_index As String
    Public ReadOnly Property index As Integer
        Get
            Return z_index
        End Get
    End Property
    Private z_Name As String
    Public ReadOnly Property Name As String
        Get
            Return z_Name
        End Get
    End Property
    Private z_Coords As Coordinates
    Public ReadOnly Property Coords As Coordinates
        Get
            Return z_Coords
        End Get
    End Property

    Public Carrots As New List(Of HL7_Carrot)
    Public Sub AddCarrot(ByVal carrot As HL7_Carrot)
        Do While Carrots.Count < carrot.index
            Carrots.Add(New HL7_Carrot(Carrots.Count, "emptyC", "", Nothing))
        Loop
        Carrots(carrot.index - 1) = carrot ' New HL7_Carrot(carrot_name, value)
    End Sub

    Public ReadOnly Property FieldText As String
        Get
            Dim FText As New StringBuilder
            For Each c In Carrots
                FText.Append(c.v)
                FText.Append("^")
            Next
            FText.Remove(FText.Length - 1, 1)
            Return FText.ToString
        End Get
    End Property

    Private Sub PopulateTieInsByFieldName()
        Throw New NotImplementedException
    End Sub

End Class