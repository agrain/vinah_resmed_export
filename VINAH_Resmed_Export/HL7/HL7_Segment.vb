﻿Imports System.Text
Public Class HL7_Segment
    Public Sub New(ByVal seg_name As String)
        z_segment_name = seg_name
    End Sub

    Private z_segment_name As String
    Public ReadOnly Property segment_name As String
        Get
            Return z_segment_name
        End Get
    End Property

    Public Fields As New List(Of List(Of HL7_Field))
    Public Sub AddField(ByVal field As HL7_Field)
        Do While Fields.Count < field.index
            Fields.Add(New List(Of HL7_Field))
        Loop
        Fields(field.index - 1).Add(field)
    End Sub

    Public ReadOnly Property AllFields() As List(Of HL7_Field)
        Get
            Dim AllF As New List(Of HL7_Field)
            For Each fl In Fields
                For Each f In fl
                    AllF.Add(f)
                Next
            Next
            Return AllF
        End Get
    End Property
    Public ReadOnly Property SegmentText As String
        Get
            Dim SegText As New StringBuilder
            SegText.Append(Me.segment_name)
            If segment_name <> "MSH" Then SegText.Append("|")
            For Each f As List(Of HL7_Field) In Fields
                If f.Count > 0 Then
                    For Each rf In f
                        SegText.Append(rf.FieldText)
                        SegText.Append("~")
                    Next
                    SegText.Remove(SegText.Length - 1, 1) 'remove trailing ~
                End If
                SegText.Append("|")
            Next
            SegText.Remove(SegText.Length - 1, 1) 'remove trailing |
            Return SegText.ToString
        End Get
    End Property
End Class
