﻿Public Class HL7_Carrot
    Public Sub New(ByVal carrot_index As Integer, ByVal carrot_name As String, ByVal val As String, ByVal coords As Coordinates)
        If carrot_index = 0 Then carrot_index = 1
        z_index = carrot_index
        z_carrot_name = carrot_name
        z_value = val
        z_coords = coords
    End Sub
    Private z_index As String
    Public ReadOnly Property index As Integer
        Get
            Return z_index
        End Get
    End Property
    Private z_value As String
    Public ReadOnly Property v As String
        Get
            Return z_value
        End Get
    End Property
    Private z_carrot_name As String
    Public ReadOnly Property carrot_name As String
        Get
            Return z_carrot_name
        End Get
    End Property
    Private z_coords As Coordinates
    Public ReadOnly Property Coordinates As Coordinates
        Get
            Return z_coords
        End Get
    End Property
End Class
