﻿Public Class HL7_File
    Public Sub New()
        z_fileTimestamp = Now
    End Sub
    Private z_fileTimestamp As DateTime
    Public ReadOnly Property fileTimestamp As DateTime
        Get
            Return z_fileTimestamp
        End Get
    End Property

    Public Batches As New List(Of HL7_Batch)

    Public Sub LogFile()
        Dim EM As New ResMedEntities
        Dim newfile As New Vin_Log_File With {.CreateDate = fileTimestamp, .AcknowledgmentStatus = False}
        EM.Vin_Log_File.Add(newfile)
        EM.SaveChanges()

        Dim fileid = EM.Vin_Log_File.Last().FileID

        For Each b In Batches
            b.LogBatch(fileid)
        Next
    End Sub
End Class
