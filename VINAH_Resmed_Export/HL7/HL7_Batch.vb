﻿Public Class HL7_Batch
    Public Sub New(ByVal PatientID As Decimal)
        z_ClientID = PatientID
        z_batchTimestamp = Now
    End Sub

    Private z_ClientID As Decimal
    Public ReadOnly Property ClientID As Decimal
        Get
            Return z_ClientID
        End Get
    End Property
    Private z_batchTimestamp As DateTime
    Public ReadOnly Property batchTimestamp As DateTime
        Get
            Return z_batchTimestamp
        End Get
    End Property
    Public Messages As New List(Of HL7_Message)

    Public ReadOnly Property Valid As Boolean
        Get
            If Messages.Count > 0 Then
                Return True
            Else
                Return False
            End If
        End Get
    End Property

    Public Sub LogBatch(ByVal FileId As Integer)
        Dim EM As New ResMedEntities
        Dim newBatch As New Vin_Log_Batch With {.CreationDate = batchTimestamp, .AcknowledgmentStatus = False, .FileID = FileId, .RClientID = ClientID}
        EM.Vin_Log_Batch.Add(newBatch)
        EM.SaveChanges()

        Dim batchid = EM.Vin_Log_Batch.Last().BatchID

        For Each m In Messages
            m.LogMessage(batchID)
        Next
    End Sub
End Class
